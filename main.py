#!/usr/bin/python3
"""
	written by QueryVS
	License:GPLv1
	the app can scanning facebook user page.

"""
from bs4 import BeautifulSoup
from hashlib import md5

import requests

urlist = "./urls.lst"
url_lines=[]


if(__name__ == "__main__"):
	with open(urlist) as f:
	    url_lines=f.readlines()

	for i in range(0,len(url_lines),1):
		if(url_lines[i]!=""):
			print(md5(url_lines[i].encode()).hexdigest())
		else:
			continue
else:
	print("this is not modules, can not be used as a module ")
